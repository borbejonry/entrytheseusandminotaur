using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameView : MonoBehaviour
{
    public event Action OnRestart;
    public event Action OnIdle;
    public event Action OnUndo;

    [SerializeField] GameObject objToClone;
    [SerializeField] Transform content;

    bool isHelp = true;
    bool isMenu = true;
    [SerializeField] Button menu;
    [SerializeField] RectTransform menus;
    [SerializeField] RectTransform helpInfo;
    [SerializeField] RectTransform loseInfo;
    [SerializeField] RectTransform winInfo;

    [SerializeField] TextMeshProUGUI levelTxt;

    [SerializeField] Button helpBtnInfo;
    [SerializeField] Button helpBtn;
    [SerializeField] Button restartBtn;
    [SerializeField] Button undoBtn;
    [SerializeField] Button waitBtn;


    private void Start()
    {
        menu.onClick.AddListener(MenuClicked);
        helpBtnInfo.onClick.AddListener(HelpClicked);
        helpBtn.onClick.AddListener(HelpClicked);
        restartBtn.onClick.AddListener(RestartClicked);
        undoBtn.onClick.AddListener(UndoClicked);
        waitBtn.onClick.AddListener(WaitClicked);
    }

    public void UpdateLevelText(int p_level)
    {
        levelTxt.text = "Level " + p_level;
    }

    public void HelpClicked()
    {
        isHelp = !isHelp;
        helpInfo.gameObject.SetActive(isHelp);
    }
    private void RestartClicked()
    {
        OnRestart?.Invoke();
    }
    private void UndoClicked()
    {
        OnUndo?.Invoke();
    }
    private void WaitClicked()
    {
        OnIdle?.Invoke();
    }

    public void Win(bool isOn)
    {
        winInfo.gameObject.SetActive(isOn);
    }

    public void Lose(bool isOn)
    {
        loseInfo.gameObject.SetActive(isOn);
    }

    void MenuClicked()
    {
        isMenu = !isMenu;
        if (isMenu)
        {
            menus.anchorMin = new Vector2(0, 0);
            menus.anchorMax = new Vector2(0.25f, 1);
        }
        else
        {
            menus.anchorMin = new Vector2(0, 0);
            menus.anchorMax = new Vector2(0, 1);

            if (isHelp)
            {
                HelpClicked();
            }
        }
    }

    public void CreateObjects(Blocks[,] p_data, int p_maxX, int p_maxY)
    {
        for (int y = 0; y < p_maxY; y++)
        {
            for (int x = 0; x < p_maxX; x++)
            {
                GameObject _obj = Instantiate(objToClone, content);
                _obj.transform.localPosition = new Vector3(x, y, 0);
                _obj.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                _obj.name = "block " + x + ":" + y;
                BlockGameObject _blockObj = _obj.GetComponent<BlockGameObject>();
                p_data[x, y].index = new Vector2(x, y);
                p_data[x, y].SetGameObject(_obj);
                _blockObj.block = p_data[x, y];
            }
        }

    }
    public void UpdateAreaColor(Blocks[,] p_data, int p_maxX, int p_maxY)
    {
        for (int y = 0; y < p_maxY; y++)
        {
            for (int x = 0; x < p_maxX; x++)
            {
                p_data[x, y].UpdateAreaColor();
            }
        }
    }
    public void UpdateAreaColor(Blocks p_prev, Blocks p_new)
    {
        p_prev.UpdateAreaColor();
        p_new.UpdateAreaColor();
    }
}
