using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Blocks// : MonoBehaviour
{
    public enum AREA
    {
        PLANE,
        WALL,
        FINISH,
        PLAYER,
        ENEMY
    }

    public AREA areaType;
    public Vector2 index;

    GameObject obj;

    public void SetExistingValue(Blocks data)
    {
        areaType = data.areaType;
        index = data.index;

        Debug.Log("Set player: " + index + " " + areaType);

        if (data.GetGameObject() != null)
        {
            obj = data.GetGameObject();
        }
    }

    public void SetGameObject(GameObject p_obj)
    {
        obj = p_obj;
    }
    public GameObject GetGameObject()
    {
        return obj;
    }

    public void SetAreaType(AREA type)
    {
        areaType = type;
    }

    public void UpdateAreaColor()
    {
        switch (areaType)
        {
            case AREA.ENEMY: obj.GetComponent<MeshRenderer>().material.color = Color.red; break;
            case AREA.PLAYER: obj.GetComponent<MeshRenderer>().material.color = Color.yellow; break;
            case AREA.FINISH: obj.GetComponent<MeshRenderer>().material.color = Color.green; break;
            case AREA.WALL: obj.GetComponent<MeshRenderer>().material.color = Color.black; break;
            case AREA.PLANE: obj.GetComponent<MeshRenderer>().material.color = Color.gray; break;
            default: break;
        }
    }
}
