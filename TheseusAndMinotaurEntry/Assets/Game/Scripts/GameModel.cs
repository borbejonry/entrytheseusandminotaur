using System.Collections;
using System.Collections.Generic;

public class GameModel
{
    List<Blocks> playerMovementHistory;
    List<Blocks> enemyMovementHistory;

    public enum MOVEMENT
    {
        UP, DOWN, LEFT, RIGHT
    }

    public int maxX;
    public int maxY;

    public Blocks[,] blocks;

    public Blocks player;
    public Blocks enemy;

    public bool isPlayerTurn;
    public bool isGameStop;

    public int currentLevel;

    public Blocks GetPrevEnemyHistory()
    {

        if ((enemyMovementHistory.Count == 0) || (enemyMovementHistory == null))
        {
            return null;
        }

        Blocks newData = new Blocks();
        newData.SetExistingValue(enemyMovementHistory[enemyMovementHistory.Count - 1]);

        enemyMovementHistory.RemoveAt(enemyMovementHistory.Count - 1);

        return newData;
    }

    public void AddEnemyMovement(Blocks data)
    {
        if (enemyMovementHistory == null)
        {
            enemyMovementHistory = new List<Blocks>();
        }

        Blocks newData = new Blocks();
        newData.SetExistingValue(data);

        enemyMovementHistory.Add(newData);
    }

    public void AddPlayerMovement(Blocks data)
    {
        if(playerMovementHistory == null)
        {
            playerMovementHistory = new List<Blocks>();
        }

        Blocks newData = new Blocks();
        newData.SetExistingValue(data);

        playerMovementHistory.Add(newData);
    }

    public Blocks GetPrevPlayerHistory()
    {

        if ((playerMovementHistory.Count == 0)||(playerMovementHistory == null))
        {
            return null;
        }

        Blocks newData = new Blocks();
        newData.SetExistingValue(playerMovementHistory[playerMovementHistory.Count - 1]);

        playerMovementHistory.RemoveAt(playerMovementHistory.Count - 1);

        return newData;
    }
    public void ClearMovement()
    {
        if (playerMovementHistory != null)
        {
            playerMovementHistory.Clear();
        }
        if (enemyMovementHistory != null)
        {
            enemyMovementHistory.Clear();
        }
    }

    public Blocks[,] GetBlocks()
    {
        return blocks;
    }
    public Blocks SearchBlock(int x, int y)
    {
        return blocks[x, y];
    }

    public void CreateBlocks()
    {
        blocks = new Blocks[maxX, maxY];

        for (int y = 0; y < maxX; y++)
        {
            for (int x = 0; x < maxY; x++)
            {
                Blocks block = new Blocks();
                block.areaType = Blocks.AREA.PLANE;
                blocks[x, y] = block;
            }
        }
    }

    void SetAllAreaToPlanee()
    {
        for (int y = 0; y < maxX; y++)
        {
            for (int x = 0; x < maxY; x++)
            {
                blocks[x, y].areaType = Blocks.AREA.PLANE;
            }
        }
    }

    void SetPlayerAndEnemy(int p_level)
    {

        if (player == null)
        {
            player = new Blocks();
        }

        if (enemy == null)
        {
            enemy = new Blocks();
        }

        switch (p_level)
        {
            case 4:
                blocks[2, 2].SetAreaType(Blocks.AREA.PLAYER);
                player.SetExistingValue(blocks[2, 2]);

                blocks[11, 11].SetAreaType(Blocks.AREA.ENEMY);
                enemy.SetExistingValue(blocks[11, 11]);
                break;
            case 3:
                blocks[2, 2].SetAreaType(Blocks.AREA.PLAYER);
                player.SetExistingValue(blocks[2, 2]);

                blocks[8, 2].SetAreaType(Blocks.AREA.ENEMY);
                enemy.SetExistingValue(blocks[8, 2]);
                break;
            case 2:
                blocks[8, 5].SetAreaType(Blocks.AREA.PLAYER);
                player.SetExistingValue(blocks[8, 5]);

                blocks[8, 2].SetAreaType(Blocks.AREA.ENEMY);
                enemy.SetExistingValue(blocks[8, 2]);
                break;
            default:
                blocks[6, 6].SetAreaType(Blocks.AREA.PLAYER);
                player.SetExistingValue(blocks[6, 6]);

                blocks[6, 2].SetAreaType(Blocks.AREA.ENEMY);
                enemy.SetExistingValue(blocks[6, 2]);
                break;
        }
    }

    public void SetLevelOne()
    {
        SetAllAreaToPlanee();
        currentLevel = 1;
        SetPlayerAndEnemy(currentLevel);

        blocks[8, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[7, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[4, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 4].SetAreaType(Blocks.AREA.FINISH);
        blocks[8, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 2].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[7, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[4, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 2].SetAreaType(Blocks.AREA.WALL);
    }

    public void SetLevelTwo()
    {
        SetAllAreaToPlanee();

        currentLevel = 2;
        SetPlayerAndEnemy(currentLevel);

        blocks[9, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[7, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[4, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 8].SetAreaType(Blocks.AREA.WALL);

        blocks[6, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 3].SetAreaType(Blocks.AREA.WALL);

        blocks[5, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 4].SetAreaType(Blocks.AREA.FINISH);
        blocks[5, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[7, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[4, 1].SetAreaType(Blocks.AREA.WALL);

        blocks[8, 1].SetAreaType(Blocks.AREA.WALL);

        blocks[3, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 2].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 1].SetAreaType(Blocks.AREA.WALL);

        blocks[7, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[7, 3].SetAreaType(Blocks.AREA.WALL);

        blocks[9, 1].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 2].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 7].SetAreaType(Blocks.AREA.WALL);
    }
    public void SetLevelThree()
    {
        SetAllAreaToPlanee();

        currentLevel = 3;
        SetPlayerAndEnemy(currentLevel);

        for (int i = 0; i < 10; i++)
        {
            blocks[i, 7].SetAreaType(Blocks.AREA.WALL);
        }
        for (int i = 0; i < 8; i++)
        {
            blocks[9, i].SetAreaType(Blocks.AREA.WALL);
        }

        for (int i = 0; i < 9; i++)
        {
            blocks[i, 0].SetAreaType(Blocks.AREA.WALL);
        }

        for (int i = 0; i < 8; i++)
        {
            blocks[0, i].SetAreaType(Blocks.AREA.WALL);
        }

        blocks[4, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[2, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 2].SetAreaType(Blocks.AREA.WALL);

        blocks[6, 0].SetAreaType(Blocks.AREA.FINISH);
    }
    public void SetLevelFour()
    {
        SetAllAreaToPlanee();

        currentLevel = 4;
        SetPlayerAndEnemy(currentLevel);

        for (int i = 0; i < 15; i++)
        {
            blocks[i, 0].SetAreaType(Blocks.AREA.WALL);
            blocks[i, 14].SetAreaType(Blocks.AREA.WALL);
            blocks[14, i].SetAreaType(Blocks.AREA.WALL);
            blocks[0, i].SetAreaType(Blocks.AREA.WALL);
        }

        blocks[4, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[2, 5].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 2].SetAreaType(Blocks.AREA.WALL);

        blocks[4, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[6, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[5, 8].SetAreaType(Blocks.AREA.WALL);

        blocks[9, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[10, 4].SetAreaType(Blocks.AREA.WALL);
        blocks[11, 3].SetAreaType(Blocks.AREA.WALL);
        blocks[12, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[11, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[10, 7].SetAreaType(Blocks.AREA.WALL);
        blocks[9, 6].SetAreaType(Blocks.AREA.WALL);
        blocks[8, 5].SetAreaType(Blocks.AREA.WALL);

        blocks[2, 12].SetAreaType(Blocks.AREA.WALL);
        blocks[3, 11].SetAreaType(Blocks.AREA.WALL);
        blocks[4, 11].SetAreaType(Blocks.AREA.WALL);

        blocks[2, 8].SetAreaType(Blocks.AREA.WALL);
        blocks[2, 9].SetAreaType(Blocks.AREA.WALL);

        blocks[12, 12].SetAreaType(Blocks.AREA.FINISH);
    }
}
