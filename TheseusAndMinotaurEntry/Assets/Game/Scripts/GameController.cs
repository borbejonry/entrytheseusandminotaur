using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] GameView viewManager;

    GameModel main;

    Blocks[,] _blocks;

    bool _isEnemyMovedRight = false;
    bool _isEnemyMovedUp = false;

    bool isWon = false;



    // Start is called before the first frame update
    void Start()
    {
        viewManager.OnRestart += RestartClicked;
        viewManager.OnIdle += IdleClicked;
        viewManager.OnUndo += UndoClicked;
        main = new GameModel();
        main.currentLevel = 1;
        main.maxX = 15;
        main.maxY = 15;
        main.isPlayerTurn = true;
        main.isGameStop = false;
        main.CreateBlocks();
        viewManager.CreateObjects(main.GetBlocks(), main.maxX, main.maxY);

        GameStart(main.currentLevel);

        _blocks = main.GetBlocks();
    }

    private void UndoClicked()
    {
        Blocks _tempHistory = main.GetPrevPlayerHistory();
        Blocks _tempHistoryEnemy = main.GetPrevEnemyHistory();
        if (_tempHistory != null)
        {

            Blocks _curMainBlockEnemy = new Blocks();
            _curMainBlockEnemy.SetExistingValue(main.enemy);
            _curMainBlockEnemy.SetAreaType(Blocks.AREA.PLANE);
            _blocks[(int)_curMainBlockEnemy.index.x, (int)_curMainBlockEnemy.index.y].SetAreaType(Blocks.AREA.PLANE);
            _tempHistoryEnemy.SetAreaType(Blocks.AREA.ENEMY);
            main.enemy.SetExistingValue(_tempHistoryEnemy);
            viewManager.UpdateAreaColor(_curMainBlockEnemy, _tempHistoryEnemy);

            Blocks _curMainBlock = new Blocks();
            _curMainBlock.SetExistingValue(main.player);
            _curMainBlock.SetAreaType(Blocks.AREA.PLANE);
            _blocks[(int)_curMainBlock.index.x, (int)_curMainBlock.index.y].SetAreaType(Blocks.AREA.PLANE);
            _tempHistory.SetAreaType(Blocks.AREA.PLAYER);
            main.player.SetExistingValue(_tempHistory);
            viewManager.UpdateAreaColor(_curMainBlock, _tempHistory);
        }
    }

    private void IdleClicked()
    {
        main.AddPlayerMovement(main.player);

        _isEnemyMovedUp = false;
        _isEnemyMovedRight = false;

        main.isPlayerTurn = false;

        StartCoroutine(EnemyMovement());
    }

    private void RestartClicked()
    {
        GameStart(main.currentLevel);
    }

    void GameStart(int p_level = 1)
    {
        main.ClearMovement();
        main.isPlayerTurn = true;
        main.isGameStop = false;

        switch (p_level)
        {
            case 4:
                main.SetLevelFour();
                break;
            case 3:
                main.SetLevelThree();
                break;
            case 2:
                main.SetLevelTwo();
                break;
            default: //level 1
                main.SetLevelOne();
                break;
        }
        viewManager.UpdateAreaColor(main.GetBlocks(), main.maxX, main.maxY);

        viewManager.UpdateLevelText(p_level);
    }

    // Update is called once per frame
    void Update()
    {
        if (main.isGameStop)
        {
            if (Input.anyKeyDown)
            {
                if (isWon)
                {
                    if (main.currentLevel <= 4)
                    {
                        main.currentLevel++;

                        if (main.currentLevel >= 5)
                        {
                            main.currentLevel = 1;
                        }
                    }
                    else
                    {
                        main.currentLevel = 1;
                    }
                }
                else
                {
                    main.currentLevel = 1;
                }

                GameStart(main.currentLevel);

                viewManager.Win(false);
                viewManager.Lose(false);
            }
        }
        else
        {
            if (main.isPlayerTurn)
            {
                if (Input.GetKeyDown(KeyCode.I))
                {
                    IdleClicked();
                }
                else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
                {
                    //up
                    MovePlayer(GameModel.MOVEMENT.UP);
                }
                else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    //down
                    MovePlayer(GameModel.MOVEMENT.DOWN);
                }
                else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    //left
                    Debug.Log("left pressed");
                    MovePlayer(GameModel.MOVEMENT.LEFT);
                }
                else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                {
                    //right
                    MovePlayer(GameModel.MOVEMENT.RIGHT);
                }
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                GameStart(main.currentLevel);
            }
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            viewManager.HelpClicked();
        }
    }
    void MovePlayer(GameModel.MOVEMENT type)
    {

        int _pX = (int)main.player.index.x;
        int _pY = (int)main.player.index.y;

        switch (type)
        {
            case GameModel.MOVEMENT.UP:
                CheckPlayerMovement(_blocks[_pX, _pY + 1], main.player);
                break;
            case GameModel.MOVEMENT.DOWN:
                CheckPlayerMovement(_blocks[_pX, _pY - 1], main.player);
                break;
            case GameModel.MOVEMENT.LEFT:
                CheckPlayerMovement(_blocks[_pX - 1, _pY], main.player);
                break;
            case GameModel.MOVEMENT.RIGHT:
                CheckPlayerMovement(_blocks[_pX + 1, _pY], main.player);
                break;
            default: break;
        }
    }

    void CheckPlayerMovement(Blocks p_target, Blocks p_player)
    {
        Debug.Log("left pressed " + p_target.index + " : " + p_player.index + " " + p_target.areaType);

        Blocks _blockTarget = _blocks[(int)p_target.index.x, (int)p_target.index.y];
        Blocks _blockPrev = _blocks[(int)p_player.index.x, (int)p_player.index.y];


        switch (p_target.areaType)
        {
            case Blocks.AREA.FINISH:
                _isEnemyMovedUp = false;
                _isEnemyMovedRight = false;

                _blockTarget.SetAreaType(Blocks.AREA.PLAYER);
                _blockPrev.SetAreaType(Blocks.AREA.PLANE);

                p_player.SetExistingValue(_blockTarget);

                StopCoroutine(EnemyMovement());
                Debug.Log("player win");
                PlayerWin();
                break;
            case Blocks.AREA.ENEMY:
                Debug.Log("player lose");
                _blockTarget.SetAreaType(Blocks.AREA.ENEMY);
                _blockPrev.SetAreaType(Blocks.AREA.PLANE);

                p_player.SetExistingValue(_blockTarget);

                StopCoroutine(EnemyMovement());
                Debug.Log("player lose");
                PlayerLose();
                break;
            case Blocks.AREA.PLANE:

                main.AddPlayerMovement(p_player);

                _isEnemyMovedUp = false;
                _isEnemyMovedRight = false;

                main.isPlayerTurn = false;
                _blockTarget.SetAreaType(Blocks.AREA.PLAYER);
                _blockPrev.SetAreaType(Blocks.AREA.PLANE);

                p_player.SetExistingValue(_blockTarget);

                StartCoroutine(EnemyMovement());
                break;
            case Blocks.AREA.WALL:
                //do nothing
                break;
            default:
                break;
        }

        viewManager.UpdateAreaColor(_blockPrev, p_player);
    }
    

    void MoveEnemy()
    {

        int _pX = (int)main.enemy.index.x;
        int _pY = (int)main.enemy.index.y;

        if (main.player.index.x > main.enemy.index.x)
        {
            //move enemy right
            _isEnemyMovedRight = CheckEnemyMovementPossible(_blocks[_pX + 1, _pY], main.enemy);
            if (_isEnemyMovedRight)
            {
                //enemy moved successfull
            }
            else
            {
                //check enemy vertical
                MoveEnemyVertical();
            }
            
        }
        else if (main.player.index.x < main.enemy.index.x)
        {
            //move enemy left
            _isEnemyMovedRight = CheckEnemyMovementPossible(_blocks[_pX - 1, _pY], main.enemy);
            if (_isEnemyMovedRight)
            {
                //enemy moved successfull
                _isEnemyMovedRight = false;
            }
            else
            {
                //check enemy vertical
                MoveEnemyVertical();
            }
        }
        else
        {
            if( MoveEnemyVertical())
            {
            }
        }
    }

    bool MoveEnemyVertical()
    {
        Debug.Log("vertical");
        bool _isTemp = false;

        int _pX = (int)main.enemy.index.x;
        int _pY = (int)main.enemy.index.y;

        if (main.player.index.y > main.enemy.index.y)
        {
            //move enemy up
            _isEnemyMovedUp = CheckEnemyMovementPossible(_blocks[_pX, _pY + 1], main.enemy);
            if (_isEnemyMovedUp)
            {
                _isTemp = true;
            }
            else
            {
                _isTemp = false;
            }
        }
        else if (main.player.index.y < main.enemy.index.y)
        {
            //move enemy down
            _isEnemyMovedUp = CheckEnemyMovementPossible(_blocks[_pX, _pY - 1], main.enemy);
            if (_isEnemyMovedUp)
            {
                _isEnemyMovedUp = false;
                _isTemp = true;
            }
            else
            {
                _isTemp = false;
            }
        }
        else
        {
            _isTemp = false;
        }

        return _isTemp;
    }

    bool CheckEnemyMovementPossible(Blocks p_target, Blocks p_enemy)
    {
        bool _isMove = false;

        Blocks _blockTarget = _blocks[(int)p_target.index.x, (int)p_target.index.y];
        Blocks _blockPrev = _blocks[(int)p_enemy.index.x, (int)p_enemy.index.y];

        switch (p_target.areaType)
        {
            case Blocks.AREA.FINISH:
                //do nothing

                break;
            case Blocks.AREA.PLAYER:

                _blockTarget.SetAreaType(Blocks.AREA.ENEMY);
                _blockPrev.SetAreaType(Blocks.AREA.PLANE);

                p_enemy.SetExistingValue(_blockTarget);

                StopCoroutine(EnemyMovement());
                Debug.Log("player lose");
                PlayerLose();

                break;
            case Blocks.AREA.PLANE:

                _blockTarget.SetAreaType(Blocks.AREA.ENEMY);
                _blockPrev.SetAreaType(Blocks.AREA.PLANE);

                p_enemy.SetExistingValue(_blockTarget);

                _isMove = true;
                break;
            case Blocks.AREA.WALL:
                //do nothing
                break;
            default:
                break;
        }

        viewManager.UpdateAreaColor(_blockPrev, p_enemy);

        return _isMove;
    }
    IEnumerator EnemyMovement()
    {
        main.AddEnemyMovement(main.enemy);

        yield return new WaitForSeconds(0.25f);
        MoveEnemy();

        yield return new WaitForSeconds(0.25f);
        MoveEnemy();

        main.isPlayerTurn = true;
    }

    void PlayerLose()
    {
        isWon = false;
        main.isGameStop = true;
        viewManager.Lose(true);
    }
    void PlayerWin()
    {
        isWon = true;
        main.isGameStop = true;
        viewManager.Win(true);
    }
}
